A simple no-HTML random Hatsune Miku image server. Example: https://rms.owo69.me/

- Click reload to get a different one. Delete left half of URL to get to source.
- Include pornographic results: https://rms.owo69.me/?allow_r18([=only](https://rms.owo69.me/?allow_r18=only))
- Get different sizes (original, regular, small, or thumb_mini): https://rms.owo69.me/?size=thumb_mini
- Auto-refresh using HTTP Refresh header: https://rms.owo69.me/?auto=3
- kek: https://rms.owo69.me/?allow_r18&size=thumb_mini&auto=0
- See your history: https://rms.owo69.me/log
- Load image by id without redirect thing: https://rms.owo69.me/img/51586149
- Load random image directly without redirect thing: https://rms.owo69.me/img/random
- Draw Pixiv URL onto upper left corner of image: https://rms.owo69.me/img/random?drawsource&size=regular

Here is an HTML site that makes use of it: https://lamp.tk/miku.html

### Prerequisites

- Node.js
- GraphicsMagick for ?drawsource option

### todo

- Option to limit range to get more higher-ranked illustrations
